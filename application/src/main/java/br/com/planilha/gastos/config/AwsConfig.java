package br.com.planilha.gastos.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.planilha.gastos.entity.TransactionEntity;
import br.com.planilha.gastos.entity.UserEntity;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.sns.SnsClient;

@Configuration
public class AwsConfig {

	@Value("${aws.region}")
	private String region;

	@Value("${aws.dynammodb.table.transaction}")
	private String transactionTableName;
	
	@Value("${aws.dynammodb.table.user}")
	private String userTableName;

	@Bean
	public SnsClient snsClient() {
		return SnsClient.builder().region(Region.of(region)).build();
	}

	@Bean
	public DynamoDbEnhancedClient dynamoDbClient() {
		DynamoDbClient dynamoDbClient = DynamoDbClient.builder().region(Region.of(region)).build();
		return DynamoDbEnhancedClient.builder().dynamoDbClient(dynamoDbClient).build();
	}

	@Bean
	public DynamoDbTable<TransactionEntity> dynamoDbTransactionTable() {
		return dynamoDbClient().table(transactionTableName, TableSchema.fromClass(TransactionEntity.class));
	}
	
	@Bean
	public DynamoDbTable<UserEntity> dynamoDbUserTable() {
		return dynamoDbClient().table(userTableName, TableSchema.fromClass(UserEntity.class));
	}

}
