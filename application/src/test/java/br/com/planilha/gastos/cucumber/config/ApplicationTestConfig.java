package br.com.planilha.gastos.cucumber.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import br.com.planilha.gastos.entity.TransactionEntity;
import br.com.planilha.gastos.entity.UserEntity;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.sns.SnsClient;

@Configuration
@Profile("test")
public class ApplicationTestConfig {

	@Value("${aws.region}")
	private String region;

	@Value("${aws.dynammodb.endpoint}")
	private String dynamoDbEndpoint;
	
	@Value("${aws.dynammodb.table.transaction}")
	private String transactionTableName;
	
	@Value("${aws.dynammodb.table.user}")
	private String userTableName;

	@Bean
	@Primary
	public SnsClient snsClientMock() {
		return Mockito.mock(SnsClient.class);
	}

	@Bean
	@Primary
	public DynamoDbClient dynamoDbClientMock() throws URISyntaxException {
		return DynamoDbClient.builder().region(Region.of(region)).endpointOverride(new URI(dynamoDbEndpoint)).build();
	}
	
	@Bean
	@Primary
	public DynamoDbEnhancedClient dynamoDbEnhancedClientMock() throws URISyntaxException {
		return DynamoDbEnhancedClient.builder().dynamoDbClient(dynamoDbClientMock()).build();
	}

	@Bean
	@Primary
	public DynamoDbTable<TransactionEntity> dynamoDbTransactionTableMock() throws URISyntaxException {
		return dynamoDbEnhancedClientMock().table(transactionTableName, TableSchema.fromClass(TransactionEntity.class));
	}
	
	@Bean
	@Primary
	public DynamoDbTable<UserEntity> dynamoDbUserTableMock() throws URISyntaxException {
		return dynamoDbEnhancedClientMock().table(userTableName, TableSchema.fromClass(UserEntity.class));
	}

}
