package br.com.planilha.gastos.cucumber.mock;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;

import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeDefinition;
import software.amazon.awssdk.services.dynamodb.model.CreateTableRequest;
import software.amazon.awssdk.services.dynamodb.model.GlobalSecondaryIndex;
import software.amazon.awssdk.services.dynamodb.model.KeySchemaElement;
import software.amazon.awssdk.services.dynamodb.model.KeyType;
import software.amazon.awssdk.services.dynamodb.model.Projection;
import software.amazon.awssdk.services.dynamodb.model.ProjectionType;
import software.amazon.awssdk.services.dynamodb.model.ProvisionedThroughput;

@Component
@Profile("test")
public class LocalDynamoDb {

	protected DynamoDBProxyServer server;
	public static String port;

	public LocalDynamoDb() {
		System.setProperty("sqlite4java.library.path", "native-libs");
	}

	@Autowired
	private DynamoDbClient dynamoDbClient;
	
	public void run() throws Exception {
		stop();
		start();
		createTables();
	}
	
	public void start() throws Exception {
		port = "8000";
		this.server = ServerRunner.createServerFromCommandLineArgs(new String[] { "-inMemory", "-port", port });
		server.start();
	}

	public void stop() {
		this.stopUnchecked(server);
	}

	protected void stopUnchecked(DynamoDBProxyServer dynamoDbServer) {
		try {
			dynamoDbServer.stop();
		} catch (Exception e) {
		}
	}

	public void createTables() {
		createUserTable();
		createTransactionsTable();
	}

	private void createTransactionsTable() {
		ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();

		attributeDefinitions.add(AttributeDefinition
				.builder()
				.attributeName("id")
				.attributeType("S")
				.build());
		
		attributeDefinitions.add(AttributeDefinition
				.builder()
				.attributeName("user_id")
				.attributeType("S")
				.build());
		
		attributeDefinitions.add(AttributeDefinition
				.builder()
				.attributeName("data")
				.attributeType("S")
				.build());

		ArrayList<KeySchemaElement> tableKeySchema = new ArrayList<KeySchemaElement>();
		tableKeySchema.add(KeySchemaElement
				.builder()
				.attributeName("id")
				.keyType(KeyType.HASH)
				.build());

		// transactions_by_user_id index
		ArrayList<KeySchemaElement> indexKeySchema = new ArrayList<KeySchemaElement>();

		indexKeySchema.add(KeySchemaElement
				.builder()
				.attributeName("user_id")
				.keyType(KeyType.HASH)
				.build()); 
		
		indexKeySchema.add(KeySchemaElement
				.builder()
				.attributeName("data")
				.keyType(KeyType.RANGE)
				.build()); 

		GlobalSecondaryIndex transactionsByUserId = GlobalSecondaryIndex
				.builder()
				.indexName("transactions_by_user_id")
				.provisionedThroughput(ProvisionedThroughput
						.builder()
						.readCapacityUnits((long) 1)
						.writeCapacityUnits((long) 1)
						.build())
				.projection(Projection
						.builder()
						.projectionType(ProjectionType.ALL)
						.build())
				.keySchema(indexKeySchema)
				.build();

		CreateTableRequest createTableRequest = CreateTableRequest
				.builder()
				.tableName("transaction-table")
				.provisionedThroughput(ProvisionedThroughput
						.builder()
						.readCapacityUnits((long) 1)
						.writeCapacityUnits((long) 1)
						.build())
				.attributeDefinitions(attributeDefinitions)
				.keySchema(tableKeySchema)
				.globalSecondaryIndexes(transactionsByUserId)
				.build();

		dynamoDbClient.createTable(createTableRequest);		
	}

	private void createUserTable() {
		ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();

		attributeDefinitions.add(AttributeDefinition
				.builder()
				.attributeName("id")
				.attributeType("S")
				.build());
		
		attributeDefinitions.add(AttributeDefinition
				.builder()
				.attributeName("email")
				.attributeType("S")
				.build());

		ArrayList<KeySchemaElement> tableKeySchema = new ArrayList<KeySchemaElement>();
		tableKeySchema.add(KeySchemaElement
				.builder()
				.attributeName("id")
				.keyType(KeyType.HASH)
				.build());

		// users_by_email index
		ArrayList<KeySchemaElement> indexKeySchema = new ArrayList<KeySchemaElement>();

		indexKeySchema.add(KeySchemaElement.builder().attributeName("email").keyType(KeyType.HASH).build()); 

		GlobalSecondaryIndex usersByEmailIndex = GlobalSecondaryIndex
				.builder()
				.indexName("users_by_email")
				.provisionedThroughput(ProvisionedThroughput
						.builder()
						.readCapacityUnits((long) 1)
						.writeCapacityUnits((long) 1)
						.build())
				.projection(Projection
						.builder()
						.projectionType(ProjectionType.ALL)
						.build())
				.keySchema(indexKeySchema)
				.build();

		CreateTableRequest createTableRequest = CreateTableRequest
				.builder()
				.tableName("user-table")
				.provisionedThroughput(ProvisionedThroughput
						.builder()
						.readCapacityUnits((long) 1)
						.writeCapacityUnits((long) 1)
						.build())
				.attributeDefinitions(attributeDefinitions)
				.keySchema(tableKeySchema)
				.globalSecondaryIndexes(usersByEmailIndex)
				.build();

		dynamoDbClient.createTable(createTableRequest);
	}

}