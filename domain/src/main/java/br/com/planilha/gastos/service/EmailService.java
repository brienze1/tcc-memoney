package br.com.planilha.gastos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.planilha.gastos.builder.MessageBuilder;
import br.com.planilha.gastos.entity.Device;
import br.com.planilha.gastos.entity.User;
import br.com.planilha.gastos.port.EmailAdapter;

@Component
public class EmailService {

	private static final String SUBJECT = "MeMoney Email Verification";
	
	@Autowired
	private MessageBuilder messageBuilder;
	
	@Autowired
	private EmailAdapter emailAdapter;
	
	public boolean sendDeviceVerificationEmail(User user, Device device) {
		String message = messageBuilder.buildDeviceVerificationMessage(user, device);
		
		return emailAdapter.send(user.getEmail(), message, SUBJECT);
	}

}
