package br.com.planilha.gastos.port;

public interface EmailAdapter {

	boolean send(String email, String message, String subject);

}
