package br.com.planilha.gastos.rules;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.planilha.gastos.entity.Transaction;
import br.com.planilha.gastos.exception.TransactionException;
import br.com.planilha.gastos.port.TransactionPersistenceAdapter;

@ExtendWith(SpringExtension.class)
public class TransactionRulesTest {

	@InjectMocks
	private TransactionRules transactionRules;
	
	@Mock
	private TransactionPersistenceAdapter transactionPersistence;
	
	private Transaction transaction;
	
	@BeforeEach
	public void init() {
		transaction = new Transaction();
		transaction.setTransactionId(UUID.randomUUID().toString());
		transaction.setValor(new BigDecimal(1.99));
		transaction.setMeioDePagamento("Cartao");
		transaction.setLocalizacao("Mercado");
		transaction.setTipo("Sent");
		transaction.setDescricao("Compras do mes");
		transaction.setData(LocalDateTime.now());
	}
	
	@Test
	public void validateTest() {
		Boolean isValid = transactionRules.validate(transaction);
		
		Assertions.assertTrue(isValid);
	}
	
	@Test
	public void validateInvalidIdTest() {
		Boolean isValid = transactionRules.validate(transaction);
		
		Assertions.assertTrue(isValid);
	}
	
	@Test
	public void validateNullFieldsTest() {
		transaction.setData(null);
		transaction.setTransactionId(null);
		transaction.setDescricao(null);
		transaction.setLocalizacao(null);
		transaction.setMeioDePagamento(null);
		transaction.setTipo(null);
		
		Boolean isValid = transactionRules.validate(transaction);
		
		Assertions.assertTrue(isValid);
		Assertions.assertEquals("Undefined", transaction.getDescricao());
		Assertions.assertEquals("Unknown", transaction.getLocalizacao());
		Assertions.assertEquals("Unknown", transaction.getMeioDePagamento());
		Assertions.assertEquals("Sent", transaction.getTipo());
		Assertions.assertNull(transaction.getTransactionId());
	}
	
	@Test
	public void validateEmptyFieldsTest() {
		transaction.setData(null);
		transaction.setDescricao(" ");
		transaction.setLocalizacao(" ");
		transaction.setMeioDePagamento(" ");
		transaction.setTipo(" ");
		transaction.setTransactionId(" ");
		
		Boolean isValid = transactionRules.validate(transaction);
		
		Assertions.assertTrue(isValid);
		Assertions.assertEquals("Undefined", transaction.getDescricao());
		Assertions.assertEquals("Unknown", transaction.getLocalizacao());
		Assertions.assertEquals("Unknown", transaction.getMeioDePagamento());
		Assertions.assertEquals("Sent", transaction.getTipo());
		Assertions.assertEquals(" ", transaction.getTransactionId());
	}
	
	@Test
	public void validateTransactionNullTest() {
		Assertions.assertThrows(
				TransactionException.class, 
				() -> transactionRules.validate(null), 
				"Transaction can't be null");
	}
	
	@Test
	public void validateValorNullTest() {
		transaction.setValor(null);
		
		Assertions.assertThrows(
				TransactionException.class, 
				() -> transactionRules.validate(transaction), 
				"Value can't be null, zero or less than zero");
	}
	
	@Test
	public void validateValorLessThenZeroTest() {
		transaction.setValor(new BigDecimal(-1));
		
		Assertions.assertThrows(
				TransactionException.class, 
				() -> transactionRules.validate(transaction), 
				"Value can't be null, zero or less than zero");
	}
	
}
