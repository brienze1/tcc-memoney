package br.com.planilha.gastos.persistence;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import br.com.planilha.gastos.entity.Device;
import br.com.planilha.gastos.entity.DeviceEntity;
import br.com.planilha.gastos.entity.Transaction;
import br.com.planilha.gastos.entity.TransactionEntity;
import br.com.planilha.gastos.entity.User;
import br.com.planilha.gastos.entity.UserEntity;
import br.com.planilha.gastos.exception.TransactionException;
import br.com.planilha.gastos.parse.TransactionIntegrationParse;
import software.amazon.awssdk.core.pagination.sync.SdkIterable;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbIndex;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.Page;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

@ExtendWith(SpringExtension.class)
public class TransactionPersistenceTest {

	private static final String TRANSACTIONS_BY_USER_ID_INDEX = "transactions_by_user_id";
	
	@InjectMocks
	private TransactionPersistence transactionPersistence;
	
	@Mock
	private DynamoDbTable<TransactionEntity> transactionRepository;
	
	@Mock
	private TransactionIntegrationParse transactionParse;
	
	@Mock
	private UserPersistence userPersistence;
	
	private Transaction transaction;
	private User user;
	private TransactionEntity transactionEntity;
	private UserEntity userEntity;
	private List<TransactionEntity> transactionsEntity;
	private List<Transaction> transactions;
	private LocalDateTime date;
	private Integer quantity;
	private Integer page;
	private DynamoDbIndex<TransactionEntity> index;
	private SdkIterable<Page<TransactionEntity>> query;
	private Iterator<Page<TransactionEntity>> iterator;
	private Page<TransactionEntity> nextPage;
	
	@SuppressWarnings("unchecked")
	@BeforeEach
	public void init() {
		transaction = new Transaction();
		transaction.setData(LocalDateTime.now());
		transaction.setDescricao(UUID.randomUUID().toString());
		transaction.setTransactionId(UUID.randomUUID().toString());
		transaction.setLocalizacao(UUID.randomUUID().toString());
		transaction.setMeioDePagamento(UUID.randomUUID().toString());
		transaction.setTipo(UUID.randomUUID().toString());
		transaction.setValor(new BigDecimal(new Random().nextDouble()));
		
		List<Device> devices = new ArrayList<>();
		devices.add(new Device(String.valueOf(new Random().nextInt(1000))));
		
		user = new User();
		user.setAutoLogin(true);
		user.setDevices(devices);
		user.setEmail(UUID.randomUUID().toString());
		user.setFirstName(UUID.randomUUID().toString());
		user.setId(UUID.randomUUID().toString());
		user.setInUseDevice(devices.get(0).getDeviceId());
		user.setLastName(UUID.randomUUID().toString());
		user.setPassword(UUID.randomUUID().toString());
		user.setSecret(UUID.randomUUID().toString());
		user.setValidEmail(true);

		DeviceEntity deviceEntity = new DeviceEntity();
		deviceEntity.setId(Integer.valueOf(devices.get(0).getDeviceId()));
		List<DeviceEntity> devicesEntity = new ArrayList<>();
		devicesEntity.add(deviceEntity);
		
		userEntity = new UserEntity();
		userEntity.setAutoLogin(user.isAutoLogin());
		userEntity.setDevices(devicesEntity);
		userEntity.setFirstName(user.getFirstName());
		userEntity.setId(user.getId());
		userEntity.setLastName(user.getLastName());
		userEntity.setPassword(user.getPassword());
		userEntity.setSecret(user.getSecret());
		userEntity.setValidEmail(user.isValidEmail());
		userEntity.setEmail(user.getEmail().toLowerCase());
		
		transactionEntity = new TransactionEntity();
		transactionEntity.setUserId(userEntity.getId());
		transactionEntity.setData(transaction.getData());
		transactionEntity.setDescricao(transaction.getDescricao());
		transactionEntity.setLocalizacao(transaction.getLocalizacao());
		transactionEntity.setMeioDePagamento(transaction.getMeioDePagamento());
		transactionEntity.setTipo(transaction.getTipo());
		transactionEntity.setValor(transaction.getValor());
		
		transactionsEntity = new ArrayList<>();
		transactionsEntity.add(transactionEntity);
		transactionsEntity.add(transactionEntity);
	
		transactions = new ArrayList<>();
		transactions.add(transaction);
		transactions.add(transaction);
		
		index = Mockito.mock(DynamoDbIndex.class);
		query = Mockito.mock(SdkIterable.class);
		iterator = Mockito.mock(Iterator.class);
		Map<String, AttributeValue> lastEvaluatedKey = new HashMap<>();
		nextPage = Page.create(transactionsEntity, lastEvaluatedKey);
		
		Mockito.when(transactionRepository.index(TRANSACTIONS_BY_USER_ID_INDEX)).thenReturn(index);
		Mockito.when(index.query(Mockito.any(QueryEnhancedRequest.class))).thenReturn(query);
		Mockito.when(query.iterator()).thenReturn(iterator);
		Mockito.when(iterator.next()).thenReturn(nextPage);
	}
	
	
	@Test
	public void saveTest() {
		Mockito.when(transactionParse.toTransactionEntity(transaction)).thenReturn(transactionEntity);
		
		transactionEntity.setUserId(userEntity.getId());
		
		Mockito.when(transactionRepository.updateItem(transactionEntity)).thenReturn(transactionEntity);
		Mockito.when(transactionParse.toTransaction(transactionEntity)).thenReturn(transaction);
		
		Transaction savedTransaction = transactionPersistence.save(transaction, user);
		
		Assertions.assertNotNull(savedTransaction);
		Assertions.assertEquals(transaction.getDescricao(), savedTransaction.getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransaction.getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransaction.getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransaction.getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransaction.getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransaction.getData());
		Assertions.assertEquals(transaction.getValor(), savedTransaction.getValor());
	}
	
	@Test
	public void findAllTest() {
		nextPage = Page.create(transactionsEntity);
		Mockito.when(iterator.next()).thenReturn(nextPage);
		Mockito.when(transactionParse.toTransactions(transactionsEntity)).thenReturn(transactions);
		
		List<Transaction> savedTransactions = transactionPersistence.findAll(user);
		
		Assertions.assertNotNull(savedTransactions);
		Assertions.assertFalse(savedTransactions.isEmpty());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(0).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(0).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(0).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(0).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(0).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(0).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(0).getValor());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(1).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(1).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(1).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(1).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(1).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(1).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(1).getValor());
	}
	
	@Test
	public void findTest() {
		Mockito.when(transactionRepository.getItem(Mockito.any(Key.class))).thenReturn(transactionEntity);
		Mockito.when(transactionParse.toTransaction(transactionEntity)).thenReturn(transaction);
		
		Transaction savedTransaction = transactionPersistence.find(transaction.getTransactionId());
		
		Assertions.assertNotNull(savedTransaction);
		Assertions.assertEquals(transaction.getDescricao(), savedTransaction.getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransaction.getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransaction.getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransaction.getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransaction.getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransaction.getData());
		Assertions.assertEquals(transaction.getValor(), savedTransaction.getValor());
	}
	
	@Test
	public void findNullTest() {
		Mockito.when(transactionRepository.getItem(Mockito.any(Key.class))).thenReturn(null);
		
		Assertions.assertThrows(
				TransactionException.class, 
				() -> transactionPersistence.find(transaction.getTransactionId()), 
				"Transaction does not exist");
	}
	
	@Test
	public void findSinceDateByQuantityTest() {
		date = LocalDateTime.now();
		quantity = new Random().nextInt(1000);
		page = 2;
		
		Mockito.when(transactionParse.toTransactions(transactionsEntity)).thenReturn(transactions);
	
		List<Transaction> savedTransactions = transactionPersistence.findSinceDateByQuantity(user, date, quantity, page);
		
		Assertions.assertNotNull(savedTransactions);
		Assertions.assertFalse(savedTransactions.isEmpty());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(0).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(0).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(0).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(0).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(0).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(0).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(0).getValor());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(1).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(1).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(1).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(1).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(1).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(1).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(1).getValor());
	}
	
	@Test
	public void findSinceDateTest() {
		date = LocalDateTime.now();
		
		Mockito.when(transactionParse.toTransactions(transactionsEntity)).thenReturn(transactions);
	
		List<Transaction> savedTransactions = transactionPersistence.findSinceDate(user, date);
		
		Assertions.assertNotNull(savedTransactions);
		Assertions.assertFalse(savedTransactions.isEmpty());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(0).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(0).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(0).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(0).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(0).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(0).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(0).getValor());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(1).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(1).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(1).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(1).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(1).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(1).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(1).getValor());
	}
	
	@Test
	public void findByQuantityTest() {
		quantity = new Random().nextInt(1000);
		page = 2;
		
		Mockito.when(transactionParse.toTransactions(transactionsEntity)).thenReturn(transactions);
	
		List<Transaction> savedTransactions = transactionPersistence.findByQuantity(user, quantity, page);
		
		Assertions.assertNotNull(savedTransactions);
		Assertions.assertFalse(savedTransactions.isEmpty());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(0).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(0).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(0).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(0).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(0).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(0).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(0).getValor());
		Assertions.assertEquals(transaction.getDescricao(), savedTransactions.get(1).getDescricao());
		Assertions.assertEquals(transaction.getTransactionId(), savedTransactions.get(1).getTransactionId());
		Assertions.assertEquals(transaction.getLocalizacao(), savedTransactions.get(1).getLocalizacao());
		Assertions.assertEquals(transaction.getMeioDePagamento(), savedTransactions.get(1).getMeioDePagamento());
		Assertions.assertEquals(transaction.getTipo(), savedTransactions.get(1).getTipo());
		Assertions.assertEquals(transaction.getData(), savedTransactions.get(1).getData());
		Assertions.assertEquals(transaction.getValor(), savedTransactions.get(1).getValor());
	}
	
}
