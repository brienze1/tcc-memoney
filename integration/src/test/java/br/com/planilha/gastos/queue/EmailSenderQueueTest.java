package br.com.planilha.gastos.queue;

import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import br.com.planilha.gastos.exception.SnsException;
import software.amazon.awssdk.http.SdkHttpResponse;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

@ExtendWith(SpringExtension.class)
public class EmailSenderQueueTest {
	
	@InjectMocks
	private EmailSenderQueue emailSenderQueue;
	
	@Mock
	private SnsClient snsClient;
	
	private String destination; 
	private String message; 
	private String subject;
	private PublishResponse result;
	
	@BeforeEach
	public void init() {
		ReflectionTestUtils.setField(emailSenderQueue, "topicArn", "arn:aws:sns:sa-east-1:123456789012:email-sender-test");

		destination = "test@gmail.com";
		message = "message";
		subject = "subject";
		
		result = (PublishResponse) PublishResponse
				.builder()
				.messageId(UUID.randomUUID().toString())
				.sdkHttpResponse(SdkHttpResponse.builder().statusCode(200).build())
				.build();
	}
	
	@Test
	public void sendTest() {
		Mockito.when(snsClient.publish(Mockito.any(PublishRequest.class))).thenReturn(result);
		
		boolean isSent = emailSenderQueue.send(destination, message, subject);
		
		Assertions.assertTrue(isSent);
	}
	
	@Test
	public void sendErrorTest() {
		Mockito.when(snsClient.publish(Mockito.any(PublishRequest.class))).thenThrow(software.amazon.awssdk.services.sns.model.SnsException.builder().build());
		
		Assertions.assertThrows(
				SnsException.class, 
				() -> emailSenderQueue.send(destination, message, subject));
	}

}
