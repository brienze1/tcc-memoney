package br.com.planilha.gastos.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSecondaryPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSecondarySortKey;

@DynamoDbBean
public class TransactionEntity {

	private String transactionId;
	private String userId;
	private String tipo;
	private BigDecimal valor;
	private LocalDateTime data;
	private String descricao;
	private String meioDePagamento;
	private String localizacao;
	
	@DynamoDbPartitionKey
	@DynamoDbAttribute("id")
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	@DynamoDbSecondaryPartitionKey(indexNames = "transactions_by_user_id")
	@DynamoDbAttribute("user_id")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String id) {
		this.userId = id;
	}
	
	@DynamoDbAttribute("tipo")
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@DynamoDbAttribute("valor")
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@DynamoDbSecondarySortKey(indexNames = "transactions_by_user_id")
	@DynamoDbAttribute("data")
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	
	@DynamoDbAttribute("descricao")
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@DynamoDbAttribute("meio_de_pagamento")
	public String getMeioDePagamento() {
		return meioDePagamento;
	}
	public void setMeioDePagamento(String meioDePagamento) {
		this.meioDePagamento = meioDePagamento;
	}
	
	@DynamoDbAttribute("localizacao")
	public String getLocalizacao() {
		return localizacao;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	
}
