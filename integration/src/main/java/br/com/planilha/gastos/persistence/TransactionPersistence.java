package br.com.planilha.gastos.persistence;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.planilha.gastos.entity.Transaction;
import br.com.planilha.gastos.entity.TransactionEntity;
import br.com.planilha.gastos.entity.User;
import br.com.planilha.gastos.exception.TransactionException;
import br.com.planilha.gastos.parse.TransactionIntegrationParse;
import br.com.planilha.gastos.port.TransactionPersistenceAdapter;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;

@Component
public class TransactionPersistence implements TransactionPersistenceAdapter {
	
	private static final String TRANSACTIONS_BY_USER_ID_INDEX = "transactions_by_user_id";

	@Autowired
	private DynamoDbTable<TransactionEntity> transactionRepository;
	
	@Autowired
	private TransactionIntegrationParse transactionParse;
	
	@Override
	public Transaction save(Transaction transaction, User user) {
		TransactionEntity transactionEntity = transactionParse.toTransactionEntity(transaction);

		transactionEntity.setUserId(user.getId());
		
		TransactionEntity savedTransaction = transactionRepository.updateItem(transactionEntity);
		
		return transactionParse.toTransaction(savedTransaction);
	}
	
	@Override
	public List<Transaction> findAll(User user) {
		List<TransactionEntity> transactionsEntity = transactionRepository
				.index(TRANSACTIONS_BY_USER_ID_INDEX)
				.query(QueryEnhancedRequest
						.builder()
						.queryConditional(QueryConditional.keyEqualTo(Key
								.builder()
								.partitionValue(user.getId()).build()))
						.build())
				.iterator()
				.next()
				.items();
		
		return transactionParse.toTransactions(transactionsEntity);
	}

	@Override
	public Transaction find(String transactionId) {
		Optional<TransactionEntity> transactionEntity = Optional.ofNullable(transactionRepository.getItem(Key.builder().partitionValue(transactionId).build()));
		
		if(transactionEntity.isPresent()) {
			return transactionParse.toTransaction(transactionEntity.get());
		}
		
		throw new TransactionException("Transaction does not exist");
	}

	@Override
	public List<Transaction> findSinceDateByQuantity(User user, LocalDateTime date, Integer quantity, Integer page) {
		Integer initialQuantity = quantity * page; 
		
        Key fromKey = Key.builder().partitionValue(user.getId()).sortValue(date.toString()).build();
		
		Map<String, AttributeValue> startKey = null;
		if(initialQuantity > 0) {
			startKey = transactionRepository
					.index(TRANSACTIONS_BY_USER_ID_INDEX)
				.query(QueryEnhancedRequest
					.builder()
					.limit(initialQuantity)
					.scanIndexForward(false)
					.queryConditional(QueryConditional.sortGreaterThan(fromKey))
					.build())
				.iterator()
				.next()
				.lastEvaluatedKey();
		}
		
		if(startKey != null || initialQuantity == 0) {
			List<TransactionEntity> transactionsEntity = transactionRepository
					.index(TRANSACTIONS_BY_USER_ID_INDEX)
					.query(QueryEnhancedRequest
							.builder()
							.limit(quantity)
							.exclusiveStartKey(startKey)
							.scanIndexForward(false)
							.queryConditional(QueryConditional.sortGreaterThan(fromKey))
							.build())
					.iterator()
					.next()
					.items();
			
			return transactionParse.toTransactions(transactionsEntity);
		}
		
		return new ArrayList<>();
	}

	@Override
	public List<Transaction> findSinceDate(User user, LocalDateTime date) {
		Key fromKey = Key.builder().partitionValue(user.getId()).sortValue(date.toString()).build();
		
		List<TransactionEntity> transactionsEntity = transactionRepository
				.index(TRANSACTIONS_BY_USER_ID_INDEX)
				.query(QueryEnhancedRequest
						.builder()
						.scanIndexForward(false)
						.queryConditional(QueryConditional.sortGreaterThan(fromKey))
						.build())
				.iterator()
				.next()
				.items();
		
		return transactionParse.toTransactions(transactionsEntity);
	}

	@Override
	public List<Transaction> findByQuantity(User user, Integer quantity, Integer page) {
		Integer initialQuantity = quantity * page; 
		
		Map<String, AttributeValue> startKey = null;
		if(initialQuantity > 0) {
			startKey = transactionRepository
					.index(TRANSACTIONS_BY_USER_ID_INDEX)
				.query(QueryEnhancedRequest
					.builder()
					.limit(initialQuantity)
					.scanIndexForward(false)
					.queryConditional(QueryConditional.keyEqualTo(Key
							.builder()
							.partitionValue(user.getId()).build()))
					.build())
				.iterator()
				.next()
				.lastEvaluatedKey();
		}
		
		if(startKey != null || initialQuantity == 0) {
			List<TransactionEntity> transactionsEntity = transactionRepository
					.index(TRANSACTIONS_BY_USER_ID_INDEX)
					.query(QueryEnhancedRequest
							.builder()
							.limit(quantity)
							.exclusiveStartKey(startKey)
							.scanIndexForward(false)
							.queryConditional(QueryConditional.keyEqualTo(Key
									.builder()
									.partitionValue(user.getId()).build()))
							.build())
					.iterator()
					.next()
					.items();
			
			return transactionParse.toTransactions(transactionsEntity);
		} 
		
		return new ArrayList<>();
	}

}
