package br.com.planilha.gastos.entity;

import java.util.List;

import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbAttribute;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbSecondaryPartitionKey;

@DynamoDbBean
public class UserEntity {

	private String id;
	private String email;
	private String password;
	private String lastName;
	private String firstName;
	private String secret;
	private List<DeviceEntity> devices;
	private boolean validEmail;
	private boolean autoLogin;

	@DynamoDbPartitionKey
    @DynamoDbAttribute("id")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@DynamoDbSecondaryPartitionKey(indexNames = "users_by_email")
	@DynamoDbAttribute("email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@DynamoDbAttribute("password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@DynamoDbAttribute("last_name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@DynamoDbAttribute("first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@DynamoDbAttribute("secret")
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}

	@DynamoDbAttribute("devices")
	public List<DeviceEntity> getDevices() {
		return devices;
	}
	public void setDevices(List<DeviceEntity> devices) {
		this.devices = devices;
	}

	@DynamoDbAttribute("valid_email")
	public boolean isValidEmail() {
		return validEmail;
	}
	public void setValidEmail(boolean validEmail) {
		this.validEmail = validEmail;
	}

	@DynamoDbAttribute("auto_login")
	public boolean isAutoLogin() {
		return autoLogin;
	}
	public void setAutoLogin(boolean autoLogin) {
		this.autoLogin = autoLogin;
	}

}
