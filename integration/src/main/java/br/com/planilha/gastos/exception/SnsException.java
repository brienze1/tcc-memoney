package br.com.planilha.gastos.exception;

public class SnsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SnsException(Exception e) {
		super(e);
	}
	
}
