package br.com.planilha.gastos.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.planilha.gastos.exception.SnsException;
import br.com.planilha.gastos.port.EmailAdapter;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;

@Component
public class EmailSenderQueue implements EmailAdapter {

	@Value("${aws.topic.email.sender.arn}")
	private String topicArn;

	@Autowired
	private SnsClient snsClient;
	
	@Override
	public boolean send(String destination, String message, String subject) {
			Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
			messageAttributes.put("subject", MessageAttributeValue.builder().dataType("String").stringValue(subject).build());
			messageAttributes.put("destination", MessageAttributeValue.builder().dataType("String").stringValue(destination).build());
			messageAttributes.put("message", MessageAttributeValue.builder().dataType("String").stringValue(message).build());

			try {
				PublishRequest request = PublishRequest
						.builder()
						.message(UUID.randomUUID().toString())
						.messageAttributes(messageAttributes)
						.topicArn(topicArn).build();

				PublishResponse result = snsClient.publish(request);
				
				System.out.println(result.messageId() + " Message sent. Status was " + result.sdkHttpResponse().statusCode());

			} catch (software.amazon.awssdk.services.sns.model.SnsException e) {
				throw new SnsException(e);
			}
			
		return true;
	}

}
