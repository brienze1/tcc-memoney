package br.com.planilha.gastos.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.planilha.gastos.entity.User;
import br.com.planilha.gastos.entity.UserEntity;
import br.com.planilha.gastos.parse.UserIntegrationParse;
import br.com.planilha.gastos.port.UserRepositoryAdapter;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.Key;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryConditional;
import software.amazon.awssdk.enhanced.dynamodb.model.QueryEnhancedRequest;

@Component
public class UserPersistence implements UserRepositoryAdapter {

	private static final String USERS_BY_EMAIL_INDEX = "users_by_email";

	@Autowired
	private DynamoDbTable<UserEntity> userRepository;

	@Autowired
	private UserIntegrationParse userIntegrationParse;

	@Override
	public Optional<User> findById(String id) {
		Optional<UserEntity> userEntity = Optional.ofNullable(userRepository.getItem(Key.builder().partitionValue(id).build()));

		return userIntegrationParse.toUser(userEntity);
	}

	public UserEntity findUserEntity(String id) {
		Optional<UserEntity> userEntity = Optional.ofNullable(userRepository.getItem(Key.builder().partitionValue(id).build()));

		return userEntity.get();
	}

	@Override
	public User save(User user) {
		UserEntity userEntity = userIntegrationParse.toUserEntity(user);

		return userIntegrationParse.toUser(userRepository.updateItem(userEntity));
	}

	@Override
	public Optional<User> findByEmail(String email) {
		List<UserEntity> usersEntity = userRepository
				.index(USERS_BY_EMAIL_INDEX)
				.query(QueryEnhancedRequest
						.builder()
						.queryConditional(QueryConditional
								.keyEqualTo(Key
										.builder()
										.partitionValue(email.toLowerCase())
										.build()))
						.build()).iterator().next().items();
		
		Optional<UserEntity> userEntity = Optional.ofNullable(null);
		if(usersEntity != null && !usersEntity.isEmpty()) {
			userEntity = Optional.ofNullable(usersEntity.get(0));
		} 
		
		return userIntegrationParse.toUser(userEntity);
	}

}
